import { Test, TestingModule } from '@nestjs/testing';
import { GoogleapiController } from './googleapi.controller';
import { GoogleapiService } from './googleapi.service';

describe('GoogleapiController', () => {
  let controller: GoogleapiController;
  let service: GoogleapiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GoogleapiController],
      providers: [GoogleapiService],
    }).compile();

    controller = module.get<GoogleapiController>(GoogleapiController);
    service = module.get<GoogleapiService>(GoogleapiService);
  });

  it('should be return an array of languages code', async () => {
    const result = ['test'];
    jest
      .spyOn(service, 'supportedLanguages')
      .mockImplementation((): any => result);

    expect(await controller.getSupportedLanguages()).toBe(result);
  });
});
