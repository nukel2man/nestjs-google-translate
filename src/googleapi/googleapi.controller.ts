import { GoogleapiService } from './googleapi.service';
import {
  Controller,
  Get,
  Query,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { translate } from './google.dto';
@ApiTags('googleapi')
@Controller('googleapi')
export class GoogleapiController {
  constructor(private googleapiService: GoogleapiService) {}
  @Get('translate')
  public async translate(@Query() query: translate): Promise<string> {
    try {
      return await this.googleapiService.translate(query.text, query.target);
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: error.message,
        },
        HttpStatus.FORBIDDEN,
        {
          cause: error.message,
        },
      );
    }
  }

  @Get('supported-languages')
  public async getSupportedLanguages(): Promise<string[]> {
    try {
      return await this.googleapiService.supportedLanguages();
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: error.message,
        },
        HttpStatus.FORBIDDEN,
        {
          cause: error.message,
        },
      );
    }
  }
}
