import { IsNotEmpty } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';
export class translate {
  @ApiProperty()
  @IsNotEmpty()
  readonly text: string;

  @ApiProperty({
    description: `Target is Languages Code, Example "en"`,
    default: `en`,
  })
  @IsNotEmpty()
  readonly target: string;
}
