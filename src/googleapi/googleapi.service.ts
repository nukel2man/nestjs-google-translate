import { Injectable } from '@nestjs/common';
import { TranslationServiceClient } from '@google-cloud/translate';

@Injectable()
export class GoogleapiService {
  async translate(text: string, target: string): Promise<string> {
    const translationClient = new TranslationServiceClient();

    // Construct request
    const request = {
      parent: `projects/${process.env.GOOGLE_PROJECT_ID}/locations/${process.env.GOOGLE_PROJECT_LOCATION}`,
      contents: [text],
      mimeType: 'text/plain', // mime types: text/plain, text/html
      sourceLanguageCode: '',
      targetLanguageCode: target,
    };

    // Run request
    const [response] = await translationClient.translateText(request);

    let res = ``;
    for (const translation of response.translations) {
      res += `${translation.translatedText} `;
    }
    return res;
  }

  async supportedLanguages(): Promise<any> {
    const translationClient = new TranslationServiceClient();
    // Construct request
    const request = {
      parent: `projects/${process.env.GOOGLE_PROJECT_ID}/locations/${process.env.GOOGLE_PROJECT_LOCATION}`,
    };
    // Get supported languages
    const [response] = await translationClient.getSupportedLanguages(request);

    return response;
  }
}
