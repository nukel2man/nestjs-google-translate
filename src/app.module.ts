import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GoogleapiController } from './googleapi/googleapi.controller';
import { GoogleapiService } from './googleapi/googleapi.service';
import { GoogleapiModule } from './googleapi/googleapi.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    GoogleapiModule,
  ],
  controllers: [AppController, GoogleapiController],
  providers: [AppService, GoogleapiService],
})
export class AppModule {}
