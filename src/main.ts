import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  // Swagger Setup
  const config = new DocumentBuilder()
    .setTitle('API Documents')
    .setDescription('The API Documents')
    .setVersion('1.0')
    .addTag('googleapi')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api-doc', app, document);
  // End Swagger Setup

  await app.listen(3000);
}
bootstrap();
